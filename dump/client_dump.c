#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#define PORT 5050
#define MAX_SZ 1024
#define SZ10B 100
int sock = 0, val, auth = 0;
char userlog[MAX_SZ];

int main(int argc, char const *argv[]) {
  struct sockaddr_in address;
  struct sockaddr_in serv_addr;
  char buffer[1024] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    printf("\n Socket creation error \n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    printf("\nConnection Failed \n");
    return -1;
  }

  if (argc == 6 && strcmp(argv[1], "-u") == 0 && strcmp(argv[3], "-p") == 0) {
    char tmp[SZ10B], read_c[SZ10B];
    sprintf(tmp, "4-%s-%s-%s", argv[2], argv[4], argv[5]);
    write(sock, tmp, SZ10B);
    auth = 4;
    read(sock, read_c, MAX_SZ);
    printf("%s", read_c);
  } else {
    char tmp[SZ10B], num[SZ10B];
    char read_c[SZ10B];
    sprintf(tmp, "5-%s", argv[1]);
    write(sock, tmp, SZ10B);
    auth = 5;
    read(sock, read_c, MAX_SZ);
    printf("%s", read_c);
  }
  return 0;
}
