#include <ctype.h>
#include <dirent.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#define PORT 5050
#define MAX_SZ 1024
#define sz10 100
int val, connectDB, is_validated = 0;
char *creds_file = "creds.txt";
char userlog[sz10];
char currDB[sz10];

void toupper_string(char str[]) {
  for (int i = 0; i < strlen(str); i++) {
    str[i] = toupper(str[i]);
  }
}

void login(int new_socket) {
  char temp_buff[100];
  char command[MAX_SZ];
  char username[100], password[100], *token;
  read(new_socket, temp_buff, sizeof(temp_buff));
  temp_buff[strlen(temp_buff) - 1] = '\0';
  sprintf(command, "grep -w '%s' %s\n", temp_buff, creds_file);

  FILE *fp = popen(command, "r");
  char buffer[MAX_SZ];

  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0) {
      char *u = strtok(temp_buff, "-");
      strcpy(userlog, u);
      break;
    }
  }
}

void login_dump(char u[sz10], char p[sz10], char creds[sz10]) {
  char path[sz10], command[sz10];
  sprintf(path, "databases/%s/user.txt", currDB);
  sprintf(command, "grep -w '%s-%s' %s\n", u, p, creds);

  FILE *fp = popen(command, "r");
  char buffer[MAX_SZ];
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0) {
      is_validated = 1;
      break;
    } else {
      is_validated = 0;
    }
  }
}

void regis_handler(int new_socket) {
  char sql[sz10] = {};
  val = read(new_socket, sql, sz10);
  char *token;
  char u[sz10], p[sz10];
  int i = 1;
  token = strtok(sql, " ");

  while (token != NULL) {
    if (i != 3 && i != 6) {
      toupper_string(token);
    }

    if (i == 1 && strcmp(token, "CREATE")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 2 && strcmp(token, "USER")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 3) {
      strcpy(u, token);
    } else if (i == 4 && strcmp(token, "IDENTIFIED")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 5 && strcmp(token, "BY")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 6) {
      strcpy(p, token);
    }

    if (i != 5)
      token = strtok(NULL, " ");
    else
      token = strtok(NULL, ";");
    i++;
  }
  char fmtCreds[sz10], command[sz10];
  sprintf(fmtCreds, "%s-%s", u, p);
  sprintf(command, "grep -w '%s' %s\n", fmtCreds, creds_file);
  FILE *fp = popen(command, "r");
  char buffer[MAX_SZ];
  int flag = 0;
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0) {
      write(new_socket, "Username already exist!", sz10);
      flag = 1;
      break;
    }
  }
  if (!flag) {
    fp = fopen(creds_file, "a");
    fprintf(fp, "%s\n", fmtCreds);
    printf("User %s Registered\n", u);
    write(new_socket, "User registered", sz10);
    fclose(fp);
  }
}

void createDB_handler(int new_socket, char sql[sz10]) {
  char *token, path[sz10], _path[sz10];

  token = strtok(sql, " ");
  toupper_string(token);
  if (strcmp(token, "CREATE")) {
    write(new_socket, "Failed create database!", sz10);
    return;
  }
  token = strtok(NULL, " ");
  toupper_string(token);
  if (strcmp(token, "DATABASE")) {
    write(new_socket, "Failed create database!", sz10);
    return;
  }
  token = strtok(NULL, ";");
  strcpy(currDB, token);
  sprintf(path, "databases/%s", token);
  mkdir(path, 0777);
  sprintf(_path, "%s/user.txt", path);
  FILE *fp = fopen(_path, "a");
  fprintf(fp, "%s\n", userlog);
  printf("Database created by %s\n", userlog);
  write(new_socket, "Databases created!", sz10);
  fclose(fp);
}

void useDB_handler(int new_socket, char s[sz10]) {
  char *token, path[sz10], cmd[sz10];
  token = strtok(s, " ");
  toupper_string(token);
  if (strcmp(token, "USE")) {
    write(new_socket, "Failed use database!", sz10);
    return;
  }
  token = strtok(NULL, ";");
  if (!strcmp(userlog, "root")) {
    strcpy(currDB, token);
    connectDB = 1;
    printf("User %s logged to database %s\n", userlog, currDB);
    write(new_socket, "User logged to database", sz10);
    return;
  } else {
    char buffer[sz10];
    int flag = 0;
    sprintf(cmd, "grep -w '%s' databases/%s/user.txt", userlog, token);
    FILE *fp = popen(cmd, "r");
    while (fgets(buffer, sizeof(buffer), fp)) {
      if (strlen(buffer) > 0) {
        strcpy(currDB, token);
        connectDB = 1;
        printf("User %s logged to database %s\n", userlog, currDB);
        write(new_socket, "User logged to database", sz10);
        flag = 1;
        break;
      }
    }
    if (!flag) {
      write(new_socket, "User can't logged to database", sz10);
    }
  }
}

void permissionDB_handler(int new_socket, char s[sz10]) {
  char *token, db[sz10], u[sz10];
  int i = 1;
  token = strtok(s, " ");
  while (token != NULL) {
    if (i != 3 && i != 5) {
      toupper_string(token);
    }
    if (i == 1 && strcmp(token, "GRANT")) {
      write(new_socket, "Wrong grant permission user syntax!", sz10);
      return;
    } else if (i == 2 && strcmp(token, "PERMISSION")) {
      write(new_socket, "Wrong grant permission user syntax!", sz10);
      return;
    } else if (i == 3) {
      strcpy(db, token);
    } else if (i == 4 && strcmp(token, "INTO")) {
      write(new_socket, "Wrong grant permission user syntax!", sz10);
      return;
    } else if (i == 5) {
      strcpy(u, token);
    }
    if (i != 4)
      token = strtok(NULL, " ");
    else
      token = strtok(NULL, ";");
    i++;
  }
  char cmd[sz10], path[sz10], _path[sz10];
  sprintf(_path, "databases/%s", db);
  DIR *dir = opendir(_path);
  if (!dir) {
    write(new_socket, "Database is not exist", sz10);
    return;
  } else {
    closedir(dir);
  }
  printf("User %s permission granted for database %s\n", u, db);
  write(new_socket, "Permission granted", sz10);
  sprintf(path, "databases/%s/user.txt", db);
  FILE *fp = fopen(path, "a");
  fprintf(fp, "%s\n", u);
  fclose(fp);
}

void createTable_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database!", sz10);
    return;
  }
  char str[sz10];
  strcpy(str, s);
  char *namaTabel = strtok(s, " ");
  for (int i = 0; i < 2; i++) {
    namaTabel = strtok(NULL, " ");
  }
  char file[sz10 * 2] = {};
  sprintf(file, "databases/%s/%s.csv", currDB, namaTabel);
  FILE *fp = fopen(file, "r");
  if (fp) {
    write(new_socket, "Tabel already exist!", sz10);
    fclose(fp);
    return;
  }
  fclose(fp);
  char *p = strstr(str, "(");
  char kolom[sz10][MAX_SZ] = {}, tipe[sz10][MAX_SZ] = {}, temp[sz10] = {};
  int i, idx = 0;
  for (int i = 1, j = 0; i < strlen(p) - 1; i++) {
    char c = p[i];
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||
        (c >= '0' && c <= '9')) {
      char cStr[sz10];
      sprintf(cStr, "%c", c);
      strcat(temp, cStr);
    } else if (strlen(temp)) {
      if (j & 1) {
        strcpy(tipe[idx], temp);
        idx++;
      } else {
        strcpy(kolom[idx], temp);
      }
      j++;
      strcpy(temp, "");
    }
  }
  fp = fopen(file, "a");
  char text[MAX_SZ], temp_t[sz10];
  for (int i = 0; i < idx; i++) {
    sprintf(temp_t, "%s:%s,", kolom[i], tipe[i]);
    strcat(text, temp_t);
  }
  text[strlen(text) - 1] = '\n';
  fprintf(fp, "%s", text);
  fclose(fp);
  memset(text, 0, sz10);
  printf("Tabel created by %s\n", userlog);
  write(new_socket, "Succes create table", sz10);
}

void dropDB_handler(int new_socket, char s[sz10]) {
  char *token;
  token = strtok(s, " ");
  toupper_string(token);
  if (strcmp(token, "DROP")) {
    write(new_socket, "Failed drop database!", sz10);
    return;
  }
  token = strtok(NULL, " ");
  toupper_string(token);
  if (strcmp(token, "DATABASE")) {
    write(new_socket, "Failed drop database!", sz10);
    return;
  }
  token = strtok(NULL, ";");
  char path[sz10], cmd[sz10];
  sprintf(path, "databases/%s", token);
  DIR *dir = opendir(path);
  if (!dir) {
    write(new_socket, "Database is not exist!", sz10);
    return;
  } else {
    closedir(dir);
  }

  if (strcmp(userlog, "root") == 0) {
    sprintf(cmd, "rm -rf %s", path);
    pid_t pid = fork();
    if (pid == 0) {
      system(cmd);
    }
    int status;
    pid_t childPid = waitpid(pid, &status, 0);
    printf("Database %s dropped by %s\n", token, userlog);
    write(new_socket, "Database dropped", sz10);
    return;
  } else {
    sprintf(cmd, "rm -rf %s", path);
    pid_t pid = fork();
    if (pid == 0) {
      system(cmd);
    }
    int status;
    pid_t childPid = waitpid(pid, &status, 0);
    printf("Database %s dropped by %s\n", token, userlog);
    write(new_socket, "Database dropped", sz10);
    return;
  }
}

void dropTable_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
  char *token;
  token = strtok(s, " ");
  toupper_string(token);
  if (strcmp(token, "DROP")) {
    write(new_socket, "Failed drop table", sz10);
  }
  token = strtok(NULL, " ");
  toupper_string(token);
  if (strcmp(token, "TABLE")) {
    write(new_socket, "Failed drop table", sz10);
  }
  token = strtok(NULL, ";");
  char path[sz10];
  sprintf(path, "databases/%s/%s.csv", currDB, token);
  FILE *fp = fopen(path, "r");
  if (!fp) {
    write(new_socket, "Table is not exist!", sz10);
    return;
  } else {
    char cmd[sz10];
    sprintf(cmd, "rm %s", path);
    pid_t pid = fork();
    if (pid == 0) {
      system(cmd);
    }
    int status;
    pid_t childPid = waitpid(pid, &status, 0);
    printf("Table %s dropped by %s\n", token, userlog);
    write(new_socket, "Table dropped", sz10);
    return;
  }
}

void dropColumn_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
  char *token;
  token = strtok(s, " ");
  int i = 1;
  char column[sz10], table[sz10];
  while (token != NULL) {
    if (i != 3 && i != 5) {
      toupper_string(token);
    }
    if (i == 1 && strcmp(token, "DROP")) {
      write(new_socket, "Wrong drop column user syntax!", sz10);
      return;
    } else if (i == 2 && strcmp(token, "COLUMN")) {
      write(new_socket, "Wrong drop column user syntax!", sz10);
      return;
    } else if (i == 3) {
      strcpy(column, token);
    } else if (i == 4 && strcmp(token, "FROM")) {
      write(new_socket, "Wrong drop column user syntax!", sz10);
      return;
    } else if (i == 5) {
      strcpy(table, token);
    }
    if (i != 4)
      token = strtok(NULL, " ");
    else
      token = strtok(NULL, ";");
    i++;
  }
  char cmd[sz10], delcmd[sz10], tmpcmd[sz10], cmd_rewrite[MAX_SZ],
      table_path[sz10], tmp_path[sz10];
  sprintf(table_path, "databases/%s/%s.csv", currDB, table);
  sprintf(tmp_path, "databases/%s/%s2.csv", currDB, table);
  sprintf(cmd, "cat %s", table_path);
  FILE *fp = popen(cmd, "r");
  FILE *tmp_fp = fopen(tmp_path, "a");
  char buffer[MAX_SZ], cols[MAX_SZ][sz10];
  int flag = 0, j = 0;
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0 && !flag) {
      char *token;
      token = strtok(buffer, "\t");
      int i = 0;
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        if (strstr(token, column) == NULL) {
          sprintf(tmp, "%s,", token);
          strcat(cmd_rewrite, tmp);
        }
        token = strtok(NULL, ",");
      }
      cmd_rewrite[strlen(cmd_rewrite) - 1] = '\0';
      flag = 1;
      fprintf(tmp_fp, "%s", cmd_rewrite);
      memset(cmd_rewrite, 0, sz10);
      fclose(tmp_fp);
    } else if (flag) {
      FILE *tmp_fp = fopen(tmp_path, "a");
      char *token, column_tmp[MAX_SZ];
      token = strtok(buffer, "\t");
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        sprintf(tmp, "%s,", token);
        strcat(column_tmp, tmp);
        token = strtok(NULL, ",");
      }
      column_tmp[strlen(column_tmp) - 1] = '\0';
      fprintf(tmp_fp, "%s", column_tmp);
      memset(column_tmp, 0, sizeof(column_tmp));
      fclose(tmp_fp);
    }
  }
  pclose(fp);
  remove(table_path);
  rename(tmp_path, table_path);
  printf("User %s remove column %s of database %s\n", userlog, table, currDB);
  write(new_socket, "Success remove column", sz10);
}

void insertColumn_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
  char str[sz10] = {};
  strcpy(str, s);
  char *namaTabel = strtok(str, " ");
  for (int i = 0; i < 2; i++) {
    namaTabel = strtok(NULL, " ");
  }
  char file[sz10 * 2] = {};
  sprintf(file, "databases/%s/%s.csv", currDB, namaTabel);
  char kolom[sz10][MAX_SZ] = {}, tipe[sz10][MAX_SZ] = {}, temp[sz10] = {};
  char *p = strstr(s, "(");
  size_t length = strlen(p);
  memmove(p, p + 1, length - 2);
  p[length - 2] = '\0';
  char *token, val[sz10][MAX_SZ];
  token = strtok(p, ",");
  strcpy(val[0], token);
  int i = 1;
  while (token != NULL) {
    token = strtok(NULL, ",");
    if (token[strlen(token) - 1] == ')') {
      token[strlen(token) - 1] = '\0';
      strcpy(val[i], token);
      i++;
      break;
    } else {
      strcpy(val[i], token);
      i++;
    }
  }
  char fmt[sz10], temp_fmt[sz10];
  for (int j = 0; j < i; j++) {
    char temp[sz10];
    sprintf(temp, "%s,", val[j]);
    strcat(fmt, temp);
  }
  fmt[strlen(fmt) - 1] = '\0';
  FILE *fp = fopen(file, "a");
  fseek(fp, 0, SEEK_END);
  fprintf(fp, "%s\n", fmt);
  fclose(fp);
  memset(fmt, 0, sizeof(fmt));
  write(new_socket, "Success insert to table", sz10);
  return;
}

void updateColumn_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
  char str[sz10] = {}, table[sz10], kolom[sz10], new_val[sz10], where_col[sz10],
       where_val[sz10], token_set[sz10], token_where[sz10];
  char *token = strtok(s, " ");
  int i = 0, flag_where = 0;
  while (token != NULL) {
    if (i == 1) {
      strcpy(table, token);
    } else if (i == 3) {
      strcpy(token_set, token);
    } else if (i == 5) {
      strcpy(token_where, token);
      flag_where = 1;
      break;
    }
    if (i == 4) {
      token = strtok(NULL, ";");
    } else {
      token = strtok(NULL, " ");
    }
    i++;
  }
  char *tmp_token_set, *tmp_token_where;
  tmp_token_set = strtok(token_set, "=");
  strcpy(kolom, tmp_token_set);
  if (!flag_where) {
    tmp_token_set = strtok(NULL, ";");
  } else {
    tmp_token_set = strtok(NULL, "\n");
  }
  strcpy(new_val, tmp_token_set);

  if (flag_where) {
    tmp_token_where = strtok(token_where, "=");
    strcpy(where_col, tmp_token_where);
    tmp_token_where = strtok(NULL, ";");
    strcpy(where_val, tmp_token_where);
  }

  char cmd[sz10], delcmd[sz10], tmpcmd[sz10], cmd_rewrite[MAX_SZ],
      table_path[sz10], tmp_path[sz10];
  sprintf(table_path, "databases/%s/%s.csv", currDB, table);
  sprintf(tmp_path, "databases/%s/%s2.csv", currDB, table);
  sprintf(cmd, "cat %s", table_path);
  FILE *fp = popen(cmd, "r");
  FILE *tmp_fp = fopen(tmp_path, "a");
  char buffer[MAX_SZ], cols[MAX_SZ][sz10];
  int flag = 0, j = 0, flag_col = 0, col_idx = 0;
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0 && !flag) {
      char *token;
      token = strtok(buffer, ",");
      while (token != NULL) {
        char tmp[sz10];
        if (!flag_col) {
          col_idx++;
        }
        if (strstr(token, kolom)) {
          flag_col = 1;
        }
        j++;
        sprintf(tmp, "%s,", token);
        strcat(cmd_rewrite, tmp);
        token = strtok(NULL, ",");
      }
      int length = strlen(cmd_rewrite);
      char new_cmd[sz10];
      for (int i = 0; i < length; i++) {
        if (i == length - 1) {
          break;
        } else {
          char temp[2] = {cmd_rewrite[i], '\0'};
          strcat(new_cmd, temp);
        }
      }
      flag = 1;
      fprintf(tmp_fp, "%s", new_cmd);
      memset(cmd_rewrite, 0, sz10);
      memset(new_cmd, 0, sz10);
      fclose(tmp_fp);
    } else if (flag) {
      FILE *tmp_fp = fopen(tmp_path, "a");
      char *token, column_tmp[MAX_SZ] = {};
      int tmp_col_idx = col_idx;
      token = strtok(buffer, ",");
      if (!flag_where) {
        while (token != NULL) {
          char tmp[sz10], temp[sz10], tmp_new_val[sz10];
          tmp_col_idx--;
          if (!tmp_col_idx) {
            sprintf(tmp, "%s,", new_val);
          } else {
            sprintf(tmp, "%s,", token);
          }
          strcat(column_tmp, tmp);
          token = strtok(NULL, ",");
        }
        int length = strlen(column_tmp);
        char new_cmd[sz10];
        for (int i = 0; i < length; i++) {
          if (i == length - 1) {
            break;
          } else {
            char temp[2] = {column_tmp[i], '\0'};
            strcat(new_cmd, temp);
          }
        }
        fprintf(tmp_fp, "%s\n", new_cmd);
        memset(new_cmd, 0, sz10);
        memset(column_tmp, 0, sz10);
        fclose(tmp_fp);
      } else {
        int idx = 0, match_col = 0;
        char str[j][sz10], cmd[sz10];
        while (token != NULL) {
          strcpy(str[idx], token);
          if (!strcmp(token, where_val)) {
            match_col = 1;
          }
          idx++;
          token = strtok(NULL, ",");
        }
        if (match_col) {
          for (int i = 0; i < idx; i++) {
            char tmp[sz10];
            if (i == tmp_col_idx - 1) {
              sprintf(tmp, "%s,", new_val);
            } else {
              sprintf(tmp, "%s,", str[i]);
            }
            strcat(cmd, tmp);
          }
        } else {
          for (int i = 0; i < idx; i++) {
            char tmp[sz10];
            sprintf(tmp, "%s,", str[i]);
            strcat(cmd, tmp);
          }
        }
        cmd[strlen(cmd) - 1] = '\0';
        fprintf(tmp_fp, "%s", cmd);
        memset(cmd, 0, sz10);
        memset(str, 0, sz10);
        memset(column_tmp, 0, sz10);
        fclose(tmp_fp);
      }
    }
  }
  pclose(fp);
  remove(table_path);
  rename(tmp_path, table_path);
  printf("User %s update column %s of database %s\n", userlog, kolom, currDB);
  write(new_socket, "Success update column", sz10);
}

void deleteColumn_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
  char *token, table[sz10], *tmp, kolom[sz10], where_val[sz10];
  bool is_where = false;
  token = strtok(s, " ");
  toupper_string(token);
  if (strcmp(token, "DELETE")) {
    write(new_socket, "Wrong delete syntax", sz10);
    return;
  }
  token = strtok(NULL, " ");
  if (strcmp(token, "FROM")) {
    write(new_socket, "Wrong delete syntax", sz10);
    return;
  }
  token = strtok(NULL, ";");
  if (strstr(token, "WHERE")) {
    is_where = true;
    tmp = strtok(token, " ");
    strcpy(table, tmp);
    for (int i = 0; i < 2; i++) {
      tmp = strtok(NULL, " ");
    }
    char *tmp2;
    tmp2 = strtok(tmp, "=");
    strcpy(kolom, tmp2);
    tmp2 = strtok(NULL, "\n");
    strcpy(where_val, tmp2);
  } else {
    strcpy(table, token);
  }
  char cmd[sz10], delcmd[sz10], tmpcmd[sz10], cmd_temp[MAX_SZ] = {},
                                              table_path[sz10], tmp_path[sz10];
  sprintf(table_path, "databases/%s/%s.csv", currDB, table);
  sprintf(tmp_path, "databases/%s/%s2.csv", currDB, table);
  sprintf(cmd, "cat %s", table_path);
  FILE *fp = popen(cmd, "r");
  FILE *tmp_fp = fopen(tmp_path, "a");
  char buffer[MAX_SZ], cols[MAX_SZ][sz10];
  int flag = 0, j = 0, col_idx = 0;
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0 && !flag) {
      char *token;
      token = strtok(buffer, ",");
      int i = 0, flag_idx = 0;
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        if (strstr(token, kolom) && !flag_idx) {
          flag_idx = 1;
        }
        if (!flag_idx) {
          col_idx++;
        }
        sprintf(tmp, "%s,", token);
        strcat(cmd_temp, tmp);
        token = strtok(NULL, ",");
      }
      int length = strlen(cmd_temp);
      char new_cmd[sz10];
      for (int i = 0; i < length; i++) {
        if (i == length - 1) {
          break;
        } else {
          char temp[2] = {cmd_temp[i], '\0'};
          strcat(new_cmd, temp);
        }
      }
      col_idx++;
      printf("%s\n%d\n", new_cmd, col_idx);
      fprintf(tmp_fp, "%s", new_cmd);
      memset(new_cmd, 0, sizeof(new_cmd));
      fclose(tmp_fp);
      flag = 1;
    } else if (is_where) {
      char cmd_cols[sz10];
      int idx = col_idx;
      bool match_row = false;
      token = strtok(buffer, ",");
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        sprintf(tmp, "%s,", token);
        strcat(cmd_cols, tmp);
        idx--;
        if (!idx) {
          if (strstr(token, where_val)) {
            match_row = true;
            break;
          }
        }
        printf("%s\n", token);
        token = strtok(NULL, ",");
      }
      if (match_row) {
        memset(cmd_cols, 0, sizeof(cmd_cols));
      } else {
        FILE *tmp_fp = fopen(tmp_path, "a");
        int length = strlen(cmd_cols);
        char data_cmd[sz10];
        for (int i = 0; i < length; i++) {
          if (i == length - 1) {
            break;
          } else {
            char temp[2] = {cmd_cols[i], '\0'};
            strcat(data_cmd, temp);
          }
        }
        fprintf(tmp_fp, "%s", data_cmd);
        memset(data_cmd, 0, sizeof(data_cmd));
        memset(cmd_cols, 0, sizeof(cmd_cols));
        fclose(tmp_fp);
      }
    }
  }
  pclose(fp);
  remove(table_path);
  rename(tmp_path, table_path);
  if (is_where) {
    printf("User %s delete table %s where %s=%s\n", userlog, table, kolom,
           where_val);
    write(new_socket, "Success delete column", sz10);
  } else {
    printf("User %s delete all table %s of database %s\n", userlog, table,
           currDB);
    write(new_socket, "Success delete column", sz10);
  }
}

void selectColumn_handler(int new_socket, char s[sz10]) {
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
  char kolom[sz10][MAX_SZ], table[sz10], where_col[sz10], where_val[sz10];
  ;
  char *token = strtok(s, " ");
  token = strtok(NULL, ", ");
  int i = 0, flag_from = 0, flag_all = 0, flag_where = 0;
  while (token != NULL) {
    if (!strcmp(token, "FROM")) {
      flag_from = 1;
      token = strtok(NULL, ", ");
      if (token[strlen(token) - 1] == ';') {
        token[strlen(token) - 1] = '\0';
        strcpy(table, token);
        break;
      }
      strcpy(table, token);
    } else if (!strcmp(token, "WHERE")) {
      char *tmp;
      flag_where = 1;
      token = strtok(NULL, ";");
      tmp = strtok(token, "=");
      strcpy(where_col, tmp);
      tmp = strtok(NULL, "\n");
      strcpy(where_val, tmp);
    } else if (!flag_from) {
      if (!strcmp(token, "*")) {
        flag_all = 1;
      }
      strcpy(kolom[i], token);
      i++;
    }
    token = strtok(NULL, ", ");
  }
  char cmd[sz10], delcmd[sz10], tmpcmd[sz10], cmd_rewrite[MAX_SZ],
      table_path[sz10], tmp_path[sz10];
  sprintf(table_path, "databases/%s/%s.csv", currDB, table);
  sprintf(cmd, "cat %s", table_path);
  FILE *fp = popen(cmd, "r");
  char buffer[MAX_SZ], cols[MAX_SZ][sz10], response[sz10];
  int flag = 0, j = 0, mark_idx = 0, mark[i], col_idx = 0;
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0 && !flag) {
      char *token;
      int flag_idx = 0;
      token = strtok(buffer, ",");
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        for (int j = 0; j < i; j++) {
          if (strstr(token, kolom[j])) {
            mark[mark_idx] = 1;
            col_idx = mark_idx + 1;
            break;
          }
        }
        if (strstr(token, where_col) && !flag_idx) {
          flag_idx = 1;
        }
        mark_idx++;
        token = strtok(NULL, ",");
      }
      flag = 1;
    } else if (flag) {
      int _markidx = 0;
      int flag_col = col_idx;
      int is_match = 0;
      char *token, column_tmp[MAX_SZ], res[MAX_SZ] = {};
      token = strtok(buffer, ",");
      while (token != NULL) {
        flag_col--;
        char *p, *saveptr, tmp[sz10], str[sz10];
        if (flag_all) {
          sprintf(tmp, "%s,", token);
          strcat(res, tmp);
          is_match = 1;
        } else if (mark[_markidx] == 1) {
          sprintf(tmp, "%s,", token);
          strcat(res, tmp);
          if (flag_col == 1) {
            if (!strcmp(token, where_val) && flag_where) {
              is_match = 1;
            } 
          }
          else if(!flag_where && flag_col){
            is_match=1;
          }
        }
        _markidx++;
        token = strtok(NULL, ",");
      }
      if (is_match) {
        res[strlen(res) - 1] = '\n';
        strcat(response, res);
      }
      memset(res, 0, sizeof(res));
    }
  }
  response[strlen(response) - 1] = '\0';
  write(new_socket, response, sz10);
  memset(response, 0, sizeof(response));
  printf("success select column\n");
  pclose(fp);
  return;
}

void dumpDB_handler(int new_socket) {
  char cmd[sz10];
  sprintf(cmd, "find databases/%s | grep '.csv'", currDB);
  FILE *fp = popen(cmd, "r");
  char buffer[sz10], path_files[sz10][MAX_SZ], file_name[sz10][MAX_SZ];
  int idx = 0;
  while (fgets(buffer, sizeof(buffer), fp)) {
    strcpy(path_files[idx], buffer);
    char *token;
    token = strtok(buffer, "/");
    for (int i = 0; i < 2; i++) {
      token = strtok(NULL, "/");
      strcpy(file_name[idx], token);
    }
    idx++;
  }
  pclose(fp);
  char cmd_read[sz10], res[MAX_SZ];
  for (int j = 0; j < idx; j++) {
    sprintf(cmd, "cat %s", path_files[j]);
    FILE *fp2 = popen(cmd, "r");
    char buf[sz10];
    int flag_col = 0;
    while (fgets(buf, sizeof(buf), fp2)) {
      char temp[sz10], *token;
      if (!flag_col) {
        char cols[sz10][MAX_SZ];
        file_name[j][strlen(file_name[j]) - 1] = '\0';
        sprintf(temp, "DROP TABLE %s;\n", file_name[j]);
        strcat(res, temp);
        token = strtok(buf, ",");
        int _idx = 0;
        while (token != NULL) {
          strcpy(cols[_idx], token);
          _idx++;
          token = strtok(NULL, ",");
        }
        char create_tbl[sz10], temp_cols[sz10];
        for (int i = 0; i < _idx; i++) {
          char *col_token, col_name[sz10], col_type[sz10], fmt_col[sz10];
          col_token = strtok(cols[i], ":");
          strcpy(col_name, col_token);
          col_token = strtok(NULL, "\n");
          strcpy(col_type, col_token);
          sprintf(fmt_col, "%s %s,", col_name, col_type);
          strcat(temp_cols, fmt_col);
        }
        temp_cols[strlen(temp_cols) - 1] = '\0';
        sprintf(create_tbl, "CREATE TABLE %s (%s);\n\n", file_name[j],
                temp_cols);
        strcat(res, create_tbl);
        memset(temp_cols, 0, sz10);
        flag_col++;
      } else {
        char tmp[sz10];
        strcpy(tmp, buf);
        tmp[strlen(tmp) - 1] = '\0';
        sprintf(temp, "INSERT INTO %s (%s);\n", file_name[j], tmp);
        strcat(res, temp);
        memset(temp, 0, sz10);
      }
    }
    if (j < idx - 1)
      strcat(res, "\n\n");
    else {
      res[strlen(res) - 1] = '\0';
    }
  }
  write(new_socket, res, MAX_SZ);
}

int commandHandler(int auth, int new_socket, char s[sz10]) {
  char cmd[sz10] = {}, cmd_log[sz10];
  if (auth == 6) {
    strcpy(cmd, s);
    strcpy(cmd_log, s);
  } else {
    val = read(new_socket, cmd, sz10);
    strcpy(cmd_log, cmd);
  }
  time_t rawtime;
  struct tm *timeinfo;
  char buffer[80], log[MAX_SZ];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);

  char path[sz10];
  sprintf(path, "databases/%s/log.txt", currDB);
  sprintf(log, "%s:%s:%s\n", buffer, userlog, cmd_log);
  FILE *fp = fopen(path, "a");

  if (auth == 2 && strstr(cmd, "CREATE USER")) {
    regis_handler(new_socket);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "CREATE DATABASE")) {
    createDB_handler(new_socket, cmd);
    connectDB = 1;
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "USE ")) {
    useDB_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (auth == 2 && strstr(cmd, "GRANT PERMISSION")) {
    permissionDB_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "CREATE TABLE")) {
    createTable_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "DROP DATABASE")) {
    dropDB_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "DROP TABLE")) {
    dropTable_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "DROP COLUMN")) {
    dropColumn_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "INSERT INTO")) {
    insertColumn_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "UPDATE")) {
    updateColumn_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "DELETE")) {
    deleteColumn_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  } else if (strstr(cmd, "SELECT")) {
    selectColumn_handler(new_socket, cmd);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
  }
  return 1;
}

void *client_connect_thread(void *sock) {
  char temp_buffer[sz10] = {}, clientAuth[MAX_SZ] = {}, tmp[MAX_SZ] = {};
  int new_socket = *(int *)sock;
  int auth = 0;

  val = read(new_socket, clientAuth, MAX_SZ);
  if (strlen(clientAuth) == 1) {
    auth = atoi(clientAuth);
  } else {
    char *token, u[sz10], p[sz10];
    if (strlen(clientAuth) > 3) {
      token = strtok(clientAuth, "-");
      int i = 0;
      while (token != NULL) {
        if (!i) {
          auth = atoi(token);
        } else if (i == 1) {
          strcpy(u, token);
        } else if (i == 2) {
          strcpy(p, token);
        } else if (i == 3) {
          strcpy(currDB, token);
        }
        i++;
        token = strtok(NULL, "-");
      }
      login_dump(u, p, "creds.txt");
    } else {
      token = strtok(clientAuth, "-");
      auth = atoi(token);
      token = strtok(NULL, "\n");
      strcpy(currDB, token);
    }
  }
  if (auth == 1) {
    login(new_socket);
  } else if (auth == 2) {
    strcpy(userlog, "root");
  }
  if (auth) {
    printf("User %s logged in\n", userlog);
    if (auth == 5 || auth == 4) {
      if (!is_validated) {
        return 0;
      } else
        dumpDB_handler(new_socket);
    } else if (auth == 6) {
      char input[sz10];
      while (1) {
        read(new_socket, input, sz10);
        printf("%s\n", input);
        if (!strcmp(input, "break"))
          break;
        memset(input, 0, sz10);
      }
    } else {
      while (commandHandler(auth, new_socket, "no_input"))
        ;
    }
  }
  return NULL;
}

int main(int argc, char const *argv[]) {
  int server_fd, new_socket, valread;
  struct sockaddr_in address;
  int opt = 1;
  int addrlen = sizeof(address);
  char buffer[1024] = {0};

  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket failed");
    exit(EXIT_FAILURE);
  }

  if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))) {
    perror("setsockopt");
    exit(EXIT_FAILURE);
  }

  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  if (listen(server_fd, 3) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  printf("Waiting for client connections...\n");

  while (1) {
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                             (socklen_t *)&addrlen)) == -1) {
      perror("accept");
      exit(EXIT_FAILURE);
    }
    printf("Connection accepted\n");

    pthread_t sniffer_thread;
    int *new_sock = malloc(sizeof(int));
    *new_sock = new_socket;

    if (pthread_create(&sniffer_thread, NULL, client_connect_thread,
                       (void *)new_sock) != 0) {
      perror("could not create thread");
      free(new_sock);
      exit(EXIT_FAILURE);
    }

    // Detach the thread so that it cleans up after itself
    if (pthread_detach(sniffer_thread) != 0) {
      perror("could not detach thread");
      free(new_sock);
      exit(EXIT_FAILURE);
    }
  }

  close(server_fd); // Close the server socket

  return 0;
}
