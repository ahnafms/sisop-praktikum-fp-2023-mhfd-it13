#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#define PORT 5050
#define MAX_SZ 1024
#define SZ10B 100
int sock = 0, val, auth = 0;
char userlog[MAX_SZ];

void login(const char username[], const char password[]) {
  int status;
  char temp_buff[100];
  sprintf(temp_buff, "%s-%s\n", username, password);
  write(sock, temp_buff, sizeof(temp_buff));
}

void regist(char str[]) {
  char feedback[SZ10B] = {};
  write(sock, str, SZ10B);
  val = read(sock, feedback, SZ10B);
  printf("%s\n", feedback);
}

int usrCommand() {
  FILE *fp = fopen("./log.txt", "a");
  time_t rawtime;
  struct tm *timeinfo;
  char buffer[80], log[SZ10B];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
  char input[100];
  scanf("%[^\n]s", input);
  getchar();
  write(sock, input, SZ10B);
  sprintf(log, "%s:%s:%s", buffer, userlog, input);
  fprintf(fp, log, SZ10B);
  fclose(fp);
  memset(log, 0, SZ10B);
  if (strstr(input, "CREATE USER")) {
    if (auth == 2)
      regist(input);
    else
      printf("Permission Denied!\n");
  } else if (strstr(input, "CREATE DATABASE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "USE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "GRANT PERMISSION")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "CREATE TABLE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "DROP DATABASE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "DROP TABLE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "DROP COLUMN")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "INSERT INTO")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "UPDATE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "DELETE")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  } else if (strstr(input, "SELECT")) {
    val = read(sock, userlog, SZ10B);
    printf("%s\n", userlog);
  }
  return 1;
}

int main(int argc, char const *argv[]) {
  struct sockaddr_in address;
  struct sockaddr_in serv_addr;
  char buffer[1024] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    printf("\n Socket creation error \n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    printf("\nConnection Failed \n");
    return -1;
  }

  if (argc == 5 && strcmp(argv[1], "-u") == 0 && strcmp(argv[3], "-p") == 0) {
    auth = 1;
    write(sock, "1", MAX_SZ);
    login(argv[2], argv[4]);
    strcpy(userlog, "root");
  } else if (argc == 1) {
    auth = 2;
    printf("Root access granted!\n");
    write(sock, "2", MAX_SZ);
    strcpy(userlog, argv[2]);
  } else {
    printf("Wrong Login Format !\n");
    return 0;
  }
  if (auth) {
    while (usrCommand())
      ;
  }
  return 0;
}
