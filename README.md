# sisop-praktikum-modul-4-2023-mh-it13
## Daftar Isi ##
- [anggota kelompok](#anggota-kelompok)
- [A. Autentikasi](#Autentikasi)
- [B. Autorisasi](#Autorisasi)
- [C. Data Definiton Language](#DDL)
- [D. Data Manipulation Language](#DML)
- [E. Logging](#Logging)
- [F. Reliability](#Reliability)
- [G. Tambahan](#Tambahan)
- [H. Error Handling](#ErrorHandling)
- [I. Containerization](#Containerization)
- [J. Extra](#Extra)



## anggota kelompok

| nrp        | nama                       |
| ---------- | -------------------------- |
| 5027211038 | Ahnaf Musyaffa             |
| 5027211051 | Wisnu Adjie Saka           |
| 5027211062 | Anisa Ghina Salsabila      |

## Autentikasi
- Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.

```C
  sprintf(command, "grep -w '%s' %s\n", temp_buff, creds_file);

  FILE *fp = popen(command, "r");
  char buffer[MAX_SZ];

  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0) {
      char *u = strtok(temp_buff, "-");
      strcpy(userlog, u);
      break;
    }
  }
```
Dalam kode database.c terdapat sebuah command grep -w untuk untuk mencari user yang telah terdaftar dalam creds.txt

- Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.

```C
  if (!strcmp(userlog, "root")) {
    strcpy(currDB, token);
    connectDB = 1;
    printf("User %s logged to database %s\n", userlog, currDB);
    write(new_socket, "User logged to database", sz10);
    return;
```
Dalam kode diatas merupakan potongan kode untuk command USE DATABASE, sehingga untuk mengakses database tersebut harus login sebagai root terlebih dahulu.

- Menambahkan user (Hanya bisa dilakukan user root)

```C
  while (token != NULL) {
    if (i != 3 && i != 6) {
      toupper_string(token);
    }

    if (i == 1 && strcmp(token, "CREATE")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 2 && strcmp(token, "USER")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 3) {
      strcpy(u, token);
    } else if (i == 4 && strcmp(token, "IDENTIFIED")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 5 && strcmp(token, "BY")) {
      write(new_socket, "Wrong create user syntax!", sz10);
      return;
    } else if (i == 6) {
      strcpy(p, token);
    }

    if (i != 5)
      token = strtok(NULL, " ");
    else
      token = strtok(NULL, ";");
    i++;
  }
  fp = fopen(creds_file, "a");
  fprintf(fp, "%s\n", fmtCreds);
  printf("User %s Registered\n", u);
```
Dalam kode diatas digunakan untuk menambahkan user dengan mendaftarkan user `fprintf(fp, "%s\n", fmtCreds);`, fmtCreds adalah format username dan password yang sudah sesuai dengan creds.txt

### Hasil
![Screenshot_2023-06-24_at_11.33.25](/uploads/5816ab1268233b136079985fb2e2d528/Screenshot_2023-06-24_at_11.33.25.png)
![Screenshot_2023-06-24_at_11.34.00](/uploads/794be28ddd5cdc579a91f8cd4b93bb35/Screenshot_2023-06-24_at_11.34.00.png)
Gambar diatas adalah hasil saat root menambahkan user dan akan disimpan pada creds.txt

## Autorisasi

- Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.

```C
  if (!strcmp(userlog, "root")) {
    strcpy(currDB, token);
    connectDB = 1;
    printf("User %s logged to database %s\n", userlog, currDB);
    write(new_socket, "User logged to database", sz10);
    return;
  }
```
Dalam kode di atas digunakan untuk melakukan `USE DATABASE table1`, dan status connectDB diubah true maka artinya user sedang mengakses Database.

- Yang bisa memberikan permission atas database untuk suatu user hanya root.
```C
  while (token != NULL) {
    if (i != 3 && i != 5) {
      toupper_string(token);
    }
    if (i == 1 && strcmp(token, "GRANT")) {
      write(new_socket, "Wrong grant permission user syntax!", sz10);
      return;
    } else if (i == 2 && strcmp(token, "PERMISSION")) {
      write(new_socket, "Wrong grant permission user syntax!", sz10);
      return;
    } else if (i == 3) {
      strcpy(db, token);
    } else if (i == 4 && strcmp(token, "INTO")) {
      write(new_socket, "Wrong grant permission user syntax!", sz10);
      return;
    } else if (i == 5) {
      strcpy(u, token);
    }
    if (i != 4)
      token = strtok(NULL, " ");
    else
      token = strtok(NULL, ";");
    i++;
  }

  sprintf(path, "databases/%s/user.txt", db);
  FILE *fp = fopen(path, "a");
  fprintf(fp, "%s\n", u);
```
Dalam kode di atas mengolah inputan user dari string command menjadi string per kata untuk diambil string database dan username nya. Kemudian didaftarkan pada user.txt suatu database

- User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.
```C
  sprintf(command, "grep -w '%s-%s' %s\n", u, p, creds);

  FILE *fp = popen(command, "r");
  char buffer[MAX_SZ];
  while (fgets(buffer, sizeof(buffer), fp)) {
    if (strlen(buffer) > 0) {
      is_validated = 1;
      break;
    } else {
      is_validated = 0;
    }
  }
```
Untuk mengakses sebuah database maka sebuah user perlu terdaftar pada user.txt di suatu database. kode diatas digunakan untuk mencari apakah ada user dan pass yang tertera

### Hasil
![Screenshot_2023-06-24_at_11.37.29](/uploads/a685bd4a4823e31e7516f41a862d293d/Screenshot_2023-06-24_at_11.37.29.png)
![Screenshot_2023-06-24_at_11.37.35](/uploads/3dc50bf039f5dca0e4019b5e1ec02a32/Screenshot_2023-06-24_at_11.37.35.png)
Gambar diatas adalah hasil saat menjalankan perintah USE DATABASE dan GRANT PERMISSION yang kemudian akan terdaftarkan pada user.txt suatu database

## Data Definition Language
- Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
```C
  if (strcmp(token, "CREATE")) {
    write(new_socket, "Failed create database!", sz10);
    return;
  }
  token = strtok(NULL, " ");
  toupper_string(token);
  if (strcmp(token, "DATABASE")) {
    write(new_socket, "Failed create database!", sz10);
    return;
  }
  token = strtok(NULL, ";");
  strcpy(currDB, token);
  sprintf(path, "databases/%s", token);
  mkdir(path, 0777);
  sprintf(_path, "%s/user.txt", path);
  FILE *fp = fopen(_path, "a");
  fprintf(fp, "%s\n", userlog);
```
Pada potongan kode diatas digunakan `sprintf(_path, "%s/user.txt", path);` untuk mengakses user.txt suatu teks dan insert nama setelah user.txt

- Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
```C
  for (int i = 1, j = 0; i < strlen(p) - 1; i++) {
    char c = p[i];
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||
        (c >= '0' && c <= '9')) {
      char cStr[sz10];
      sprintf(cStr, "%c", c);
      strcat(temp, cStr);
    } else if (strlen(temp)) {
      if (j & 1) {
        strcpy(tipe[idx], temp);
        idx++;
      } else {
        strcpy(kolom[idx], temp);
      }
      j++;
      strcpy(temp, "");
    }
  }
```
Pada potongan kode diatas untuk mengambil tipe_data dan nama_kolom pada command create table
```C
  fp = fopen(file, "a");
  char text[MAX_SZ], temp_t[sz10];
  for (int i = 0; i < idx; i++) {
    sprintf(temp_t, "%s:%s,", kolom[i], tipe[i]);
    strcat(text, temp_t);
  }
  text[strlen(text) - 1] = '\n';
  fprintf(fp, "%s", text);
```
Pada potongan kode diatas digunakan untuk memasukkan kolom beserta tipenya dengan format `kolom:tipe` ke dalam table.txt

- Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.
```C
    sprintf(cmd, "rm -rf %s", path);
    pid_t pid = fork();
    if (pid == 0) {
      system(cmd);
    }
    int status;
    pid_t childPid = waitpid(pid, &status, 0);
    printf("Database %s dropped by %s\n", token, userlog);
    write(new_socket, "Database dropped", sz10);
    return;
```
Pada potongan diatas digunakan untuk menghapus suatu database sesuai dengan cmd (database, table, kolom)

### Hasil
![Screenshot_2023-06-24_at_11.54.29](/uploads/115bd12f8d9ea96e743e6ca35d9fc119/Screenshot_2023-06-24_at_11.54.29.png)
Gambar diatas adalah command saat menjalankan perintah create serta drop

## Data Manipulation Language
1. INSERT
- Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
```C
  while (token != NULL) {
    token = strtok(NULL, ",");
    if (token[strlen(token) - 1] == ')') {
      token[strlen(token) - 1] = '\0';
      strcpy(val[i], token);
      i++;
      break;
    } else {
      strcpy(val[i], token);
      i++;
    }
  }

  char fmt[sz10], temp_fmt[sz10];
  for (int j = 0; j < i; j++) {
    char temp[sz10];
    sprintf(temp, "%s,", val[j]);
    strcat(fmt, temp);
  }
  FILE *fp = fopen(file, "a");
  fseek(fp, 0, SEEK_END);
  fprintf(fp, "%s\n", fmt);
```
Pada potongan diatas diambil val setiap token dan seluruh value tersebut diiterasi dan disipan pada variable fmt dengan delimiter `,`. Kemudian fmt dimasukkan dalam line terakhir table.txt

2. UPDATE
- Hanya bisa update satu kolom per satu command
```C
    while (token != NULL) {
      char tmp[sz10], temp[sz10], tmp_new_val[sz10];
      tmp_col_idx--;
      if (!tmp_col_idx) {
        sprintf(tmp, "%s,", new_val);
      } else {
        sprintf(tmp, "%s,", temp);
      }
      strcat(column_tmp, tmp);
      token = strtok(NULL, ",");
    }
    int length = strlen(column_tmp);
    char new_cmd[sz10];
    for (int i = 0; i < length; i++) {
      if (i == length - 1) {
        break;
      } else {
        char temp[2] = {column_tmp[i], '\0'};
        strcat(new_cmd, temp);
      }
    }
    fprintf(tmp_fp, "%s\n", new_cmd);
```
Potongan kode diatas digunakan untuk mencari index suatu kolom yang sesuai dan mengganti setiap value dengan inputan yang diinginkan oleh user

3. DELETE
- Delete data yang ada di tabel
```C
    char *token;
    token = strtok(buffer, ",");
    int i = 0, flag_idx = 0;
    while (token != NULL) {
      char *p, *saveptr, tmp[sz10], str[sz10];
      if (strstr(token, kolom) && !flag_idx) {
        flag_idx = 1;
      }
      if (!flag_idx) {
        col_idx++;
      }
      sprintf(tmp, "%s,", token);
      strcat(cmd_temp, tmp);
      token = strtok(NULL, ",");
    }
    int length = strlen(cmd_temp);
    char new_cmd[sz10];
    for (int i = 0; i < length; i++) {
      if (i == length - 1) {
        break;
      } else {
        char temp[2] = {cmd_temp[i], '\0'};
        strcat(new_cmd, temp);
      }
    }
    col_idx++;
    printf("%s\n%d\n", new_cmd, col_idx);
    fprintf(tmp_fp, "%s", new_cmd);
```
Potongan kode diatas digunakan untuk mengambil nama kolom dan tipenya, kemudian membuat suatu file tmp baru dan write hanya kolom dan tipe data setelah itu file lama dihapus dan file tmp di rename menjadi file yang sesuai namanya

4. SELECT
- Menampilkan data yang ada di tabel
```C
    if (strlen(buffer) > 0 && !flag) {
      char *token;
      int flag_idx = 0;
      token = strtok(buffer, ",");
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        for (int j = 0; j < i; j++) {
          if (strstr(token, kolom[j])) {
            mark[mark_idx] = 1;
            col_idx = mark_idx + 1;
            break;
          }
        }
        if (strstr(token, where_col) && !flag_idx) {
          flag_idx = 1;
        }
        mark_idx++;
        token = strtok(NULL, ",");
      }
      flag = 1;
    } else if (flag) {
      int _markidx = 0;
      int flag_col = col_idx;
      int is_match = 0;
      char *token, column_tmp[MAX_SZ], res[MAX_SZ] = {};
      token = strtok(buffer, ",");
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        if (flag_all) {
          sprintf(tmp, "%s,", token);
          strcat(res, tmp);
          is_match=1;
        }
        else if (mark[_markidx] == 1) {
          sprintf(tmp, "%s,", token);
          strcat(res, tmp);
          if (flag_col == 1) {
            if (strstr(token, where_val)) {
              is_match = 1;
            }
          }
        }
        flag_col--;
        _markidx++;
        token = strtok(NULL, ",");
      }
```
Kode diatas digunakan untuk menandakan kolom yang diinginkan, kemudian menampilkan pada user jika kolom tersebut match dengan yang diinginkan user.

5. WHERE
- Command UPDATE, SELECT, dan DELETE bisa dikombinasikan dengan WHERE. WHERE hanya untuk satu kondisi. Dan hanya ‘=’.
```C
      else if (is_where) {
      char cmd_cols[sz10];
      int idx = col_idx;
      bool match_row = false;
      token = strtok(buffer, ",");
      while (token != NULL) {
        char *p, *saveptr, tmp[sz10], str[sz10];
        sprintf(tmp, "%s,", token);
        strcat(cmd_cols, tmp);
        idx--;
        if (!idx) {
          if (strstr(token, where_val)) {
            match_row = true;
            break;
          }
        }
        token = strtok(NULL, ",");
      }
      if (match_row) {
        memset(cmd_cols, 0, sizeof(cmd_cols));
      } else {
        FILE *tmp_fp = fopen(tmp_path, "a");
        int length = strlen(cmd_cols);
        char data_cmd[sz10];
        for (int i = 0; i < length; i++) {
          if (i == length - 1) {
            break;
          } else {
            char temp[2] = {cmd_cols[i], '\0'};
            strcat(data_cmd, temp);
          }
        }
        fprintf(tmp_fp, "%s", data_cmd);
        memset(data_cmd, 0, sizeof(data_cmd));
        memset(cmd_cols, 0, sizeof(cmd_cols));
        fclose(tmp_fp);
      }
```
Kode diatas digunakan untuk menandakan memvalidasi kondisi WHERE yang diinginkan dengan cara menandai value where dengan nama kolom, jika match maka di print.
Logic ini digunakan pada setiap command yang ada seperti SELECT, UPDATE, DELETE, dan INSERT

### Hasil
- INSERT 
![Screenshot_2023-06-24_at_11.57.52](/uploads/083170ed818932ac83c25e41194969a1/Screenshot_2023-06-24_at_11.57.52.png)
![Screenshot_2023-06-24_at_11.58.02](/uploads/d5ed0467f88cf16aa6345f1264cf98ea/Screenshot_2023-06-24_at_11.58.02.png)
Gambar diatas adalah contoh insert ke dalam suatu table

- DELETE
![Screenshot_2023-06-24_at_16.48.42](/uploads/9b1ea8298a549235e539108688a6dafb/Screenshot_2023-06-24_at_16.48.42.png)
![Screenshot_2023-06-24_at_16.48.48](/uploads/22e8ddeb81c033d79a0852249e04b173/Screenshot_2023-06-24_at_16.48.48.png)
Gambar diatas adalah contoh delete ke dalam suatu table

- UPDATE
![Screenshot_2023-06-24_at_17.03.40](/uploads/331c1141046ed6be3373575e8221bef6/Screenshot_2023-06-24_at_17.03.40.png)
![Screenshot_2023-06-24_at_17.03.47](/uploads/c6ee4575dc0a4300ea6fa0994b9f4ad7/Screenshot_2023-06-24_at_17.03.47.png)
Gambar diatas adalah contoh update suatu kolom pada table

- SELECT 
![Screenshot_2023-06-24_at_17.15.11](/uploads/35027a2f9d1ccacd0165842f82392c39/Screenshot_2023-06-24_at_17.15.11.png)
Gambar diatas adalah contoh select suatu kolom pada table

- WHERE
![Screenshot_2023-06-24_at_17.34.20](/uploads/e51f053c2b8a3ad4615f9558182356ad/Screenshot_2023-06-24_at_17.34.20.png)
Gambar diatas adalah contoh select menggunakan WHERE pada table

![Screenshot_2023-06-24_at_17.37.02](/uploads/0b81c8f2852dc20aa3e17b9b76fba4a0/Screenshot_2023-06-24_at_17.37.02.png)
![Screenshot_2023-06-24_at_17.37.06](/uploads/5873cc67aad54188909ecd45d79f9881/Screenshot_2023-06-24_at_17.37.06.png)
Gambar diatas adalah contoh update menggunakan WHERE pada table

![Screenshot_2023-06-24_at_17.39.33](/uploads/dad0bcc31f688a7404a9a206ed6b2b21/Screenshot_2023-06-24_at_17.39.33.png)
![Screenshot_2023-06-24_at_17.39.29](/uploads/0d1fe11803e10c05ad13481b1c834b4b/Screenshot_2023-06-24_at_17.39.29.png)
Gambar diatas adalah contoh delete menggunakan WHERE pada table

## Logging
- Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.
```C
  time_t rawtime;
  struct tm *timeinfo;
  char buffer[80], log[SZ10B];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
  char input[100];


  if (auth == 2 && strstr(cmd, "CREATE USER")) {
    regis_handler(new_socket);
    fprintf(fp, log, sz10);
    fclose(fp);
    memset(log, 0, sz10);
```
Pada setiap command akan ditambahkan `fprintf log` untuk menambahkan log setiap user apabile memberikan input yang match dengan command sql yang tertera
### Hasil
![Screenshot_2023-06-24_at_11.29.32](/uploads/5bc630f9abffefb983d6a9d0258253e6/Screenshot_2023-06-24_at_11.29.32.png)
Gambar diatas adalah isi dari log

## Reliability
- Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel. 
```C
   if (!flag_col) {
     char cols[sz10][MAX_SZ];
     file_name[j][strlen(file_name[j]) - 1] = '\0';
     sprintf(temp, "DROP TABLE %s;\n", file_name[j]);
     strcat(res, temp);
     token = strtok(buf, ",");
     int _idx = 0;
     while (token != NULL) {
       strcpy(cols[_idx], token);
       _idx++;
       token = strtok(NULL, ",");
     }
     char create_tbl[sz10], temp_cols[sz10];
     for (int i = 0; i < _idx; i++) {
       char *col_token, col_name[sz10], col_type[sz10], fmt_col[sz10];
       col_token = strtok(cols[i], ":");
       strcpy(col_name, col_token);
       col_token = strtok(NULL, "\n");
       strcpy(col_type, col_token);
       sprintf(fmt_col, "%s %s,", col_name, col_type);
       strcat(temp_cols, fmt_col);
     }
     temp_cols[strlen(temp_cols) - 1] = '\0';
     sprintf(create_tbl, "CREATE TABLE %s (%s);\n\n", file_name[j],
          temp_cols);
     strcat(res, create_tbl);
     memset(temp_cols, 0, sz10);
     flag_col++;
   } else {
     char tmp[sz10];
     strcpy(tmp, buf);
     tmp[strlen(tmp) - 1] = '\0';
     sprintf(temp, "INSERT INTO %s (%s);\n", file_name[j], tmp);
     strcat(res, temp);
     memset(temp, 0, sz10);
   }
```
Potongan kode diatas untuk mentranslate sebuah table sehingga pada kolom pertama akan membuat CREATE TABLE yang sesuai dengan nama kolom dan tipe data, kemudian kolom selanjutnya melakukan insert

### Hasil
![Screenshot_2023-06-24_at_11.25.24](/uploads/d170da4d17e31bbc9fc7e857be5f90dd/Screenshot_2023-06-24_at_11.25.24.png)
Gambar diatas adalah hasil melakukan dump pada suatu database

## Error Handling
Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.
```C
  if (!connectDB) {
    write(new_socket, "You're not in any database", sz10);
    return;
  }
```
Contoh ketika mencoba untuk melakukan command Data Manipulation Language tanpa menggunakan `USE DATABASE` terlebih dahulu, maka akan mengeluarkan pesan error `You're not in any database`

### Hasil
![Screenshot_2023-06-24_at_18.25.59](/uploads/367efad245468184304d616acb538edd/Screenshot_2023-06-24_at_18.25.59.png)
Gambar diatas merupakan salah satu contoh error handling pada command DML tanpa menggunakan `USE DATABASE`

## Extra
- Program client dan dump berkomunikasi dengan socket ke program utama dan tidak boleh langsung mengakses file-file di folder program utama.
```C
  struct sockaddr_in address;
  struct sockaddr_in serv_addr;
  char buffer[1024] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    printf("\n Socket creation error \n");
    return -1;
  }

  memset(&serv_addr, '0', sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
    printf("\nInvalid address/ Address not supported \n");
    return -1;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
    printf("\nConnection Failed \n");
    return -1;
  }

```
Kode diatas dapat ditemukan pada client.c dan client_dump.c untuk berkomunikasi dengan socket ke program utama dan tidak langsung mengakses file di folder program utama

## kendala
Untuk write berupa stream of messages, tidak bisa di read secara langsung karena akan ada missing message.
